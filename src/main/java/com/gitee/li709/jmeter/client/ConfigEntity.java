package com.gitee.li709.jmeter.client;

public class ConfigEntity {
    public String serverIP = "rbcore.52im.net";
    public int serverPort = 8901;
    public int localPort = 0;

    public ConfigEntity() {
    }

    public void setSenseMode(ConfigEntity.SenseMode mode) {
        int keepAliveInterval = 0;
        int networkConnectionTimeout = 0;
        switch(mode) {
        case MODE_3S:
            keepAliveInterval = 3000;
            networkConnectionTimeout = keepAliveInterval * 1 + 2000;
            break;
        case MODE_5S:
            keepAliveInterval = 5000;
            networkConnectionTimeout = keepAliveInterval * 1 + 3000;
            break;
        case MODE_10S:
            keepAliveInterval = 10000;
            networkConnectionTimeout = keepAliveInterval * 1 + 5000;
            break;
        case MODE_15S:
            keepAliveInterval = 15000;
            networkConnectionTimeout = keepAliveInterval * 1 + 5000;
            break;
        case MODE_30S:
            keepAliveInterval = 30000;
            networkConnectionTimeout = keepAliveInterval * 1 + 5000;
            break;
        case MODE_60S:
            keepAliveInterval = 60000;
            networkConnectionTimeout = keepAliveInterval * 1 + 5000;
            break;
        case MODE_120S:
            keepAliveInterval = 120000;
            networkConnectionTimeout = keepAliveInterval * 1 + 5000;
        }

        if (keepAliveInterval > 0) {
            KeepAliveDaemon.KEEP_ALIVE_INTERVAL = keepAliveInterval;
        }

        if (networkConnectionTimeout > 0) {
            KeepAliveDaemon.NETWORK_CONNECTION_TIME_OUT = networkConnectionTimeout;
        }

    }

    public static enum SenseMode {
        MODE_3S,
        MODE_5S,
        MODE_10S,
        MODE_15S,
        MODE_30S,
        MODE_60S,
        MODE_120S;

        private SenseMode() {
        }
    }
}
