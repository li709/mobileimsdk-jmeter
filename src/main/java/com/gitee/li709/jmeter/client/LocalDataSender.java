/*
 * Copyright (C) 2021  即时通讯网(52im.net) & Jack Jiang.
 * The MobileIMSDK_TCP (MobileIMSDK v6.1 TCP版) Project.
 * All rights reserved.
 *
 * > Github地址：https://github.com/JackJiang2011/MobileIMSDK
 * > 文档地址：  http://www.52im.net/forum-89-1.html
 * > 技术社区：  http://www.52im.net/
 * > 技术交流群：215477170 (http://www.52im.net/topic-qqgroup.html)
 * > 作者公众号：“即时通讯技术圈】”，欢迎关注！
 * > 联系作者：  http://www.52im.net/thread-2792-1-1.html
 *
 * "即时通讯网(52im.net) - 即时通讯开发者社区!" 推荐开源工程。
 *
 * LocalDataSender.java at 2021-8-4 21:36:39, code by Jack Jiang.
 */
package com.gitee.li709.jmeter.client;

import com.gitee.li709.jmeter.utils.Log;
import com.gitee.li709.jmeter.utils.TCPUtils;
import io.netty.channel.Channel;
import net.x52im.mobileimsdk.server.network.MBObserver;
import net.x52im.mobileimsdk.server.protocal.ErrorCode;
import net.x52im.mobileimsdk.server.protocal.Protocal;
import net.x52im.mobileimsdk.server.protocal.ProtocalFactory;
import net.x52im.mobileimsdk.server.protocal.c.PLoginInfo;
import org.jdesktop.swingworker.SwingWorker;

public class LocalDataSender {
	private final static String TAG = LocalDataSender.class.getSimpleName();

	private NettyClient nettyClient;

	public LocalDataSender(NettyClient nettyClient) {
		this.nettyClient = nettyClient;
	}


	int sendLogin(final PLoginInfo loginInfo) {
		int codeForCheck = this.checkBeforeSend();
		if (codeForCheck != ErrorCode.COMMON_CODE_OK) {
			return codeForCheck;
		}

		if (!nettyClient.getLocalSocketProvider().isLocalSocketReady()) {
			if (ClientCoreSDK.DEBUG) {
				Log.d(TAG, "【IMCORE-TCP】发送登陆指令时，socket连接未就绪，首先开始尝试发起连接（登陆指令将在连接成功后的回调中自动发出）。。。。");
			}

			MBObserver connectionDoneObserver = (sucess, extraObj) -> {
				if (sucess) {
					sendLoginImpl(loginInfo);
				} else {
					Log.w(TAG, "【IMCORE-TCP】[来自Netty的连接结果回调观察者通知]socket连接失败，本次登陆信息未成功发出！");
				}
			};
			nettyClient.getLocalSocketProvider().setConnectionDoneObserver(connectionDoneObserver);

			return nettyClient.getLocalSocketProvider().resetLocalSocket() != null ? ErrorCode.COMMON_CODE_OK : ErrorCode.ForC.BAD_CONNECT_TO_SERVER;
		} else {
			return this.sendLoginImpl(loginInfo);
		}
	}

	// 不推荐直接调用本方法实现“登陆”流程，请使用SendLoginAsync（此异步线程中包含发送登陆包之外的处理和逻辑）
	int sendLoginImpl(PLoginInfo loginInfo) {
		byte[] b = ProtocalFactory.createPLoginInfo(loginInfo).toBytes();
		int code = send(b, b.length);
		if (code == 0) {
			nettyClient.getClientCoreSDK().setCurrentLoginInfo(loginInfo);
		}

		return code;
	}

	public int sendLoginout() {
		int code = ErrorCode.COMMON_CODE_OK;
		if (nettyClient.getClientCoreSDK().isLoginHasInit()) {
			byte[] b = ProtocalFactory.createPLoginoutInfo(nettyClient.getClientCoreSDK().getCurrentLoginUserId()).toBytes();
			code = send(b, b.length);
			if (code == 0) {
				// do nothing
			}
		}

		nettyClient.getClientCoreSDK().release();

		return code;
	}

	int sendKeepAlive() {
		byte[] b = ProtocalFactory.createPKeepAlive(nettyClient.getClientCoreSDK().getCurrentLoginUserId()).toBytes();
		return send(b, b.length);
	}

	public int sendCommonData(String dataContentWidthStr, String to_user_id) {
		return sendCommonData(dataContentWidthStr, to_user_id, -1);
	}

	public int sendCommonData(String dataContentWidthStr, String to_user_id, int typeu) {
		return sendCommonData(dataContentWidthStr, to_user_id, null, typeu);
	}

	public int sendCommonData(String dataContentWidthStr, String to_user_id,String fingerPrint, int typeu) {
		return sendCommonData(dataContentWidthStr, to_user_id, true, fingerPrint, typeu);
	}

	public int sendCommonData(String dataContentWidthStr, String to_user_id,boolean QoS, String fingerPrint, int typeu) {
		return sendCommonData(ProtocalFactory.createCommonData(dataContentWidthStr, nettyClient.getClientCoreSDK().getCurrentLoginUserId(), to_user_id, QoS, fingerPrint, typeu));
	}

	public int sendCommonData(Protocal p) {
		if (p != null) {
			byte[] b = p.toBytes();
			int code = send(b, b.length);
			if (code == 0) {
				if (p.isQoS() && !nettyClient.getQoS4SendDaemon().exist(p.getFp())) {
					nettyClient.getQoS4SendDaemon().put(p);
				}
			}
			return code;
		} else {
			return ErrorCode.COMMON_INVALID_PROTOCAL;
		}
	}

	public int send(byte[] fullProtocalBytes, int dataLen) {
		int codeForCheck = this.checkBeforeSend();
		if (codeForCheck != ErrorCode.COMMON_CODE_OK) {
			return codeForCheck;
		}
		//
		// if(!ClientCoreSDK().isInitialed())
		// return ErrorCode.ForC.CLIENT_SDK_NO_INITIALED;

		Channel ds = nettyClient.getLocalSocketProvider().getLocalSocket();
		if (ds != null && ds.isActive()) {// && [ClientCoreSDK
											// sharedInstance].connectedToServer)
			return TCPUtils.send(ds, fullProtocalBytes, dataLen) ? ErrorCode.COMMON_CODE_OK
					: ErrorCode.COMMON_DATA_SEND_FAILD;
		} else {
			Log.d(TAG, "【IMCORE-TCP】scocket未连接，无法发送，本条将被忽略（dataLen=" + dataLen + "）!");
			return ErrorCode.COMMON_CODE_OK;
		}
	}

	private int checkBeforeSend() {
		if (!nettyClient.getClientCoreSDK().isInitialed()) {
			return ErrorCode.ForC.CLIENT_SDK_NO_INITIALED;
		}
		return ErrorCode.COMMON_CODE_OK;
	}

	// ------------------------------------------------------------------------------------------
	// utilities class

	public static abstract class SendCommonDataAsync extends SwingWorker<Integer, Object> {
		protected Protocal p = null;
		private NettyClient nettyClient;
		public SendCommonDataAsync(NettyClient nettyClient,String dataContentWidthStr, String to_user_id) {
			this(nettyClient,dataContentWidthStr, to_user_id, null, -1);
		}

		public SendCommonDataAsync(NettyClient nettyClient,String dataContentWidthStr, String to_user_id, int typeu) {
			this(nettyClient,dataContentWidthStr, to_user_id, null, typeu);
		}

		public SendCommonDataAsync(NettyClient nettyClient,String dataContentWidthStr, String to_user_id, String fingerPrint, int typeu) {
			this(nettyClient,ProtocalFactory.createCommonData(dataContentWidthStr,nettyClient.getClientCoreSDK().getCurrentLoginUserId(), to_user_id, true, fingerPrint, typeu));
		}

		public SendCommonDataAsync(NettyClient nettyClient,Protocal p) {
			this.nettyClient=nettyClient;
			if (p == null) {
				Log.w(TAG, "【IMCORE-TCP】无效的参数p==null!");
				return;
			}
			this.p = p;
		}


		@Override
		protected Integer doInBackground() {
			if (p != null) {
				return nettyClient.getLocalDataSender().sendCommonData(p);
			}
			return -1;
		}

		@Override
		protected void done() {
			int code = -1;
			try {
				code = get();
			} catch (Exception e) {
				Log.w(TAG, e.getMessage());
			}

			onPostExecute(code);
		}

		protected abstract void onPostExecute(Integer code);
	}

	public static class SendLoginDataAsync extends SwingWorker<Integer, Object> {

		protected PLoginInfo loginInfo = null;
		private NettyClient nettyClient;

		public SendLoginDataAsync(NettyClient nettyClient,PLoginInfo loginInfo)
		{
			this.loginInfo = loginInfo;
			this.nettyClient=nettyClient;
			nettyClient.getClientCoreSDK().init();
		}

		@Override
		protected Integer doInBackground() {
			int code = nettyClient.getLocalDataSender().sendLogin(this.loginInfo);
			return code;
		}

		@Override
		protected void done() {
			int code = -1;
			try {
				code = get();
			} catch (Exception e) {
				Log.w(TAG, e.getMessage());
			}

			onPostExecute(code);
		}

		protected void onPostExecute(Integer code) {
			if (code == 0) {
				// LocalUDPDataReciever().startup();
			} else {
				Log.d(TAG, "【IMCORE-TCP】数据发送失败, 错误码是：" + code + "！");
			}

			fireAfterSendLogin(code);
		}

		protected void fireAfterSendLogin(int code) {
			// default do nothing
		}
	}
}
