/*
 * Copyright (C) 2021  即时通讯网(52im.net) & Jack Jiang.
 * The MobileIMSDK_TCP (MobileIMSDK v6.1 TCP版) Project.
 * All rights reserved.
 *
 * > Github地址：https://github.com/JackJiang2011/MobileIMSDK
 * > 文档地址：  http://www.52im.net/forum-89-1.html
 * > 技术社区：  http://www.52im.net/
 * > 技术交流群：215477170 (http://www.52im.net/topic-qqgroup.html)
 * > 作者公众号：“即时通讯技术圈】”，欢迎关注！
 * > 联系作者：  http://www.52im.net/thread-2792-1-1.html
 *
 * "即时通讯网(52im.net) - 即时通讯开发者社区!" 推荐开源工程。
 *
 * AutoReLoginDaemon.java at 2021-8-4 21:36:39, code by Jack Jiang.
 */
package com.gitee.li709.jmeter.client;

import com.gitee.li709.jmeter.utils.Log;

import javax.swing.*;

public class AutoReLoginDaemon {

	private final static String TAG = AutoReLoginDaemon.class.getSimpleName();
	private AutoReLoginDaemon instance = null;
	public static int AUTO_RE$LOGIN_INTERVAL = 3000;// 2000;

	private boolean autoReLoginRunning = false;
	private boolean _excuting = false;
	private Timer timer = null;
	private NettyClient nettyClient;

	public AutoReLoginDaemon(NettyClient nettyClient) {
		this.nettyClient=nettyClient;
		init();
	}

	private void init() {
		timer = new Timer(AUTO_RE$LOGIN_INTERVAL, e -> run());
	}

	public void run() {
		if (!_excuting) {
			_excuting = true;
			if(ClientCoreSDK.DEBUG) {
				Log.p(TAG, "【IMCORE-TCP】自动重新登陆线程执行中, autoReLogin?"+ClientCoreSDK.autoReLogin+"...");
			}
			int code = -1;
			if (ClientCoreSDK.autoReLogin) {
				nettyClient.getLocalSocketProvider().closeLocalSocket();
				code = nettyClient.getLocalDataSender().sendLogin(nettyClient.getClientCoreSDK().getCurrentLoginInfo());
			}

			if (code == 0) {
				// LocalUDPDataReciever.getInstance().startup();
			}

			_excuting = false;
		}
	}

	public void stop() {
		if (timer != null)
			timer.stop();

		autoReLoginRunning = false;
	}

	public void start(boolean immediately) {
		stop();
		if (immediately) {
			timer.setInitialDelay(0);
		} else {
			timer.setInitialDelay(AUTO_RE$LOGIN_INTERVAL);
		}
		timer.start();
		autoReLoginRunning = true;
	}

	public boolean isautoReLoginRunning() {
		return autoReLoginRunning;
	}
}
