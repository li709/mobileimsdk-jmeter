package com.gitee.li709.jmeter.client;

import com.gitee.li709.jmeter.event.ChatBaseEvent;
import com.gitee.li709.jmeter.event.ChatMessageEvent;
import com.gitee.li709.jmeter.event.MessageQoSEvent;
import net.x52im.mobileimsdk.server.protocal.c.PLoginInfo;

public class ClientCoreSDK {
    private static final String TAG = ClientCoreSDK.class.getSimpleName();
    public static boolean DEBUG = false;
    public static boolean autoReLogin = true;
    private boolean _init = false;
    private boolean connectedToServer = true;
    private boolean loginHasInit = false;
    private PLoginInfo currentLoginInfo = null;
    private ChatBaseEvent chatBaseEvent = null;
    private ChatMessageEvent chatMessageEvent = null;
    private MessageQoSEvent messageQoSEvent = null;
    private NettyClient nettyClient;

    public ClientCoreSDK(NettyClient nettyClient) {
        this.nettyClient = nettyClient;
    }

    public void init() {
        if (!this._init) {
            this._init = true;
        }

    }

    public void release() {
        this.setConnectedToServer(false);
        nettyClient.getLocalSocketProvider().closeLocalSocket();
        nettyClient.getAutoReLoginDaemon().stop();
        nettyClient.getQoS4SendDaemon().stop();
        nettyClient.getKeepAliveDaemon().stop();
        nettyClient.getQoS4ReciveDaemon().stop();
        nettyClient.getQoS4SendDaemon().clear();
        nettyClient.getQoS4ReciveDaemon().clear();
        this._init = false;
        this.setLoginHasInit(false);
    }

    public void setCurrentLoginInfo(PLoginInfo currentLoginInfo) {
        this.currentLoginInfo = currentLoginInfo;
    }

    public PLoginInfo getCurrentLoginInfo() {
        return this.currentLoginInfo;
    }

    public void saveFirstLoginTime(long firstLoginTime) {
        if (this.currentLoginInfo != null) {
            this.currentLoginInfo.setFirstLoginTime(firstLoginTime);
        }

    }

    /** @deprecated */
    @Deprecated
    public String getCurrentLoginUserId() {
        return this.currentLoginInfo.getLoginUserId();
    }

    /** @deprecated */
    @Deprecated
    public String getCurrentLoginToken() {
        return this.currentLoginInfo.getLoginToken();
    }

    /** @deprecated */
    @Deprecated
    public String getCurrentLoginExtra() {
        return this.currentLoginInfo.getExtra();
    }

    public boolean isLoginHasInit() {
        return this.loginHasInit;
    }

    public ClientCoreSDK setLoginHasInit(boolean loginHasInit) {
        this.loginHasInit = loginHasInit;
        return this;
    }

    public boolean isConnectedToServer() {
        return this.connectedToServer;
    }

    public void setConnectedToServer(boolean connectedToServer) {
        this.connectedToServer = connectedToServer;
    }

    public boolean isInitialed() {
        return this._init;
    }

    public void setChatBaseEvent(ChatBaseEvent chatBaseEvent) {
        this.chatBaseEvent = chatBaseEvent;
    }

    public ChatBaseEvent getChatBaseEvent() {
        return this.chatBaseEvent;
    }

    public void setChatMessageEvent(ChatMessageEvent chatMessageEvent) {
        this.chatMessageEvent = chatMessageEvent;
    }

    public ChatMessageEvent getChatMessageEvent() {
        return this.chatMessageEvent;
    }

    public void setMessageQoSEvent(MessageQoSEvent messageQoSEvent) {
        this.messageQoSEvent = messageQoSEvent;
    }

    public MessageQoSEvent getMessageQoSEvent() {
        return this.messageQoSEvent;
    }
}
