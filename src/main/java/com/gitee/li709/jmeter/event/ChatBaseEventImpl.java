package com.gitee.li709.jmeter.event;/*
 * Copyright (C) 2021  即时通讯网(52im.net) & Jack Jiang.
 * The MobileIMSDK_TCP (MobileIMSDK v6.x TCP版) Project.
 * All rights reserved.
 *
 * > Github地址：https://github.com/JackJiang2011/MobileIMSDK
 * > 文档地址：  http://www.52im.net/forum-89-1.html
 * > 技术社区：  http://www.52im.net/
 * > 技术交流群：320837163 (http://www.52im.net/topic-qqgroup.html)
 * > 作者公众号：“即时通讯技术圈】”，欢迎关注！
 * > 联系作者：  http://www.52im.net/thread-2792-1-1.html
 *
 * "即时通讯网(52im.net) - 即时通讯开发者社区!" 推荐开源工程。
 *
 * ChatBaseEventImpl.java at 2021-7-7 22:06:46, code by Jack Jiang.
 */

import com.gitee.li709.jmeter.client.NettyClient;
import net.x52im.mobileimsdk.server.protocal.s.PKickoutInfo;

/**
 * 与IM服务器的连接事件在此ChatBaseEvent子类中实现即可。
 *
 * @author Jack Jiang
 * @version.1.1
 */
public class ChatBaseEventImpl implements ChatBaseEvent {
    private NettyClient nettyClient;

    public ChatBaseEventImpl(NettyClient nettyClient) {
        this.nettyClient = nettyClient;
    }

    /**
     * 本地用户的登陆结果回调事件通知。
     *
     * @param errorCode 服务端反馈的登录结果：0 表示登陆成功，否则为服务端自定义的出错代码（按照约定通常为>=1025的数）
     */
    @Override
    public void onLoginResponse(int errorCode) {
        if (errorCode == 0) {
            nettyClient.getSampleResultManager().addRawSubResult("登陆成功",200+"",nettyClient.getName()+"登陆成功",false);
            //登陆成功
        } else {
            nettyClient.getSampleResultManager().end("登陆失败",errorCode+"",nettyClient.getName()+"登陆失败错误码:"+errorCode);
            //登陆失败
        }

    }

    /**
     * 与服务端的通信断开的回调事件通知。
     * <br>
     * 该消息只有在客户端连接服务器成功之后网络异常中断之时触发。<br>
     * 导致与与服务端的通信断开的原因有（但不限于）：无线网络信号不稳定、网络切换等。
     *
     * @param errorCode 本回调参数表示表示连接断开的原因，目前错误码没有太多意义，仅作保留字段，目前通常为-1
     */
    @Override
    public void onLinkClose(int errorCode) {
        nettyClient.getSampleResultManager().end("连接断开",errorCode+"",nettyClient.getName()+"连接断开，错误码:"+errorCode);
    }

    /**
     * 本的用户被服务端踢出的回调事件通知。
     *
     * @param kickoutInfo 被踢信息对象，{@link PKickoutInfo} 对象中的 code字段定义了被踢原因代码
     */
    @Override
    public void onKickout(PKickoutInfo kickoutInfo) {

        String alertContent = "";
        if (kickoutInfo.getCode() == PKickoutInfo.KICKOUT_FOR_DUPLICATE_LOGIN) {
            alertContent = "账号已在其它地方登陆，当前会话已断开，请退出后重新登陆！";
        } else if (kickoutInfo.getCode() == PKickoutInfo.KICKOUT_FOR_ADMIN) {
            alertContent = "已被管理员强行踢出聊天，当前会话已断开！";
        } else {
            alertContent = "你已被踢出聊天，当前会话已断开（kickoutReason=" + kickoutInfo.getReason() + "）！";
        }

        nettyClient.getSampleResultManager().end("连接断开",kickoutInfo.getCode()+"",nettyClient.getName()+"错误信息:"+alertContent);

    }

}
