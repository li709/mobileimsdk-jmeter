package com.gitee.li709.jmeter;

import com.gitee.li709.jmeter.client.NettyClient;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class JmeterTest extends AbstractJavaSamplerClient {
    public static final String IP_NAME = "ip";
    public static final String PORT_NAME = "port";
    public static final String TIME_OUT_MILLISECOND_NAME = "timeOutMillisecond";
    public static final String ACCID_NAME = "loginUserId";
    public static final String TOKEN_NAME = "loginToken";
    public static final String PROTOCAL_NAME = "protocal";

    private static Map<String, NettyClient> threadLocal=new ConcurrentHashMap<>();


    public static void main(String[] args) {
        JmeterTest jmeterA = new JmeterTest();
        Arguments params =jmeterA.getDefaultParameters();
        params.removeArgument(PROTOCAL_NAME);
        params.addArgument(PROTOCAL_NAME,"{\"bridge\":false,\"type\":2,\"dataContent\":\"{\\\"content\\\":\\\"12345\\\",\\\"sessionType\\\":1,\\\"fromClientType\\\":1,\\\"fromNick\\\":\\\"li70x\\\",\\\"msgType\\\":0}\",\"from\":\"li709\",\"to\":\"10\",\"fp\":\"82b167f2-9624-4924-a629-3658b9bd8174\",\"QoS\":true,\"typeu\":3,\"sm\":-1}");

        JavaSamplerContext javaSamplerContext = new JavaSamplerContext(params);
        jmeterA.setupTest(javaSamplerContext);
        SampleResult sampleResult = jmeterA.runTest(javaSamplerContext);
        System.out.println(sampleResult.toString());
        jmeterA.teardownTest(javaSamplerContext);
    }

    @Override
    public SampleResult runTest(JavaSamplerContext javaSamplerContext) {
        NettyClient nettyClient = threadLocal.get(Thread.currentThread().getName());
        SampleResultManager sampleResultManager = nettyClient.getSampleResultManager();
        nettyClient.sendMsg(javaSamplerContext);
        return sampleResultManager.getSampleResult();
    }

    @Override
    public Arguments getDefaultParameters() {
        //默认参数
        Arguments jMeterProperties = new Arguments();
        jMeterProperties.addArgument(IP_NAME, "127.0.0.1");
        jMeterProperties.addArgument(PORT_NAME, "17002");
        jMeterProperties.addArgument(ACCID_NAME, "li709");
        jMeterProperties.addArgument(TOKEN_NAME, "tokenli709");
        jMeterProperties.addArgument(TIME_OUT_MILLISECOND_NAME, "1000");
        jMeterProperties.addArgument(PROTOCAL_NAME, "{\"typeu\":1,\"dataContent\":\"{\\\"content\\\":\\\"1\\\",\\\"sessionType\\\":0,\\\"fromClientType\\\":1,\\\"fromNick\\\":\\\"li70x\\\",\\\"msgType\\\":0}\",\"fp\":\"972f65b1-8909-45ef-a712-25dfadfb421f\",\"type\":2,\"QoS\":true,\"from\":\"li709\",\"sm\":-1,\"bridge\":false,\"to\":\"li708\"}");
        return jMeterProperties;
    }

    @Override
    public void setupTest(JavaSamplerContext context) {
        threadLocal.put(Thread.currentThread().getName(),new NettyClient(Thread.currentThread().getName(),context));
        super.setupTest(context);
    }

    @Override
    public void teardownTest(JavaSamplerContext context) {
        for (NettyClient value : threadLocal.values()) {
            value.release();
        }
        threadLocal.clear();
        super.teardownTest(context);
    }
}
